package hk.com.novare;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

public class CarRacingEventPublisher implements ApplicationEventPublisherAware {
    private ApplicationEventPublisher publisher;

    public void setApplicationEventPublisher (ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    public void publish() {
        CarRacingEvent carRacingEvent = new CarRacingEvent(this);
        publisher.publishEvent(carRacingEvent);
    }
}
