package hk.com.novare;

import org.springframework.context.ApplicationListener;

public class CarRacingEventHandler implements ApplicationListener<CarRacingEvent> {
    public void onApplicationEvent(CarRacingEvent carRacingEvent) {
        carRacingEvent.simulateCarRace();
    }
}
