package hk.com.novare;

import org.springframework.context.ApplicationEvent;

import java.util.Random;

public class CarRacingEvent extends ApplicationEvent {

    public CarRacingEvent(Object source) {
        super(source);
    }

    public void simulateCarRace() {
        System.out.println("\n======================================");
        System.out.println("CAR RACING EVENT - START!");

        Random randomNo = new Random();
        int noOfOpponents = randomNo.nextInt(5) + 6;
        int placeNo = randomNo.nextInt(noOfOpponents) + 1;
        System.out.println("\nJoining car race with "+noOfOpponents+" opponents...");

        String raceStanding = "";
        switch(placeNo) {
            case 1: raceStanding = placeNo+"ST"; break;
            case 2: raceStanding = placeNo+"ND"; break;
            case 3: raceStanding = placeNo+"RD"; break;
            default: raceStanding = placeNo+"TH";
        }
        System.out.println();
        if(placeNo>0 && placeNo<4){
            System.out.print("Congratulations! ");
        }
        System.out.println("You finished "+raceStanding+".");

        System.out.println("\nCAR RACING EVENT - END");
        System.out.println("======================================");
    }
}
