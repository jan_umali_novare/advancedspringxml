package hk.com.novare.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class CarModel {

    private String modelName;
    private EngineModel engine;
    private TransmissionModel transmission;

    public CarModel(String modelName, EngineModel engine) {
        this.modelName = modelName;
        this.engine = engine;
    }

    public CarModel(String modelName, EngineModel engine, TransmissionModel transmission) {
        this.modelName = modelName;
        this.engine = engine;
        this.transmission = transmission;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public EngineModel getEngine() {
        return engine;
    }

    public void setEngine(EngineModel engine) {
        this.engine = engine;
    }

    public TransmissionModel getTransmission() {
        return transmission;
    }

    public void setTransmission(TransmissionModel transmission) {
        this.transmission = transmission;
    }

    @PostConstruct
    public void init(){
        System.out.println("Creating your "+modelName+"...");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("Crashing your "+modelName+"...");
    }
}
