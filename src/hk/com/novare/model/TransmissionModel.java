package hk.com.novare.model;

import org.springframework.beans.factory.annotation.Required;

public class TransmissionModel {
    private String transmissionType;

    public TransmissionModel(String transmissionType) {
        this.transmissionType = transmissionType;
    }

    public String getTransmissionType() {
        return transmissionType;
    }
    @Required
    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }
}
