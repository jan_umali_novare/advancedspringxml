package hk.com.novare.model;

public class EngineModel {
    private String version;
    private String numberOfEngines;

    public EngineModel(String version, String numberOfEngines) {
        this.version = version;
        this.numberOfEngines = numberOfEngines;
    }

    public EngineModel() {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNumberOfEngines() {
        return numberOfEngines;
    }

    public void setNumberOfEngines(String numberOfEngines) {
        this.numberOfEngines = numberOfEngines;
    }
}
