package hk.com.novare;

import hk.com.novare.model.CarModel;
import hk.com.novare.service.carService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AdvancedSpring {
    @Qualifier("lamborghini")
    private static carService lamborghini;

    @Qualifier("ferrari")
    private static carService ferrari;

    @Qualifier("anyCar")
    private static CarModel anyCarModel;

    public static void main(String[] args){
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        context.registerShutdownHook();

        System.out.println();
        CarModel car = (CarModel) context.getBean("car");
        System.out.println(car.getModelName());
        System.out.println("Number of Engine/s: " + car.getEngine().getNumberOfEngines());
        System.out.println("Engine version: " + car.getEngine().getVersion());
        System.out.println("Transmission: " + car.getTransmission().getTransmissionType());

        //Demonstrate Prototype Bean Scope
        CarModel newCar = (CarModel) context.getBean("car");
        newCar.getEngine().setVersion("v15");
        System.out.println("(Prototype Scope) New Car's engine version: " + newCar.getEngine().getVersion());

        System.out.println();
        lamborghini = (carService) context.getBean("Lamborghini");
        CarModel carModel = lamborghini.assembleCar();
        System.out.println("Lamborghini");
        System.out.println("Number of Engine/s: " + carModel.getEngine().getNumberOfEngines());
        System.out.println("Engine version: " + carModel.getEngine().getVersion());
        System.out.println("Transmission: " + carModel.getTransmission().getTransmissionType());

        System.out.println();
        ferrari = (carService) context.getBean("Ferrari");
        CarModel carModel1 = ferrari.assembleCar();
        System.out.println("Ferrari");
        System.out.println("Number of Engine/s: " + carModel1.getEngine().getNumberOfEngines());
        System.out.println("Engine version: " + carModel1.getEngine().getVersion());
        System.out.println("Transmission: " + carModel1.getTransmission().getTransmissionType());

        // Read from car properties file
        System.out.println();
        System.out.println("(The following car details are read from car.properties file)");
        anyCarModel = (CarModel) context.getBean("carDetails");
        System.out.println(anyCarModel.getModelName());
        System.out.println("Number of Engine/s: " + anyCarModel.getEngine().getNumberOfEngines());
        System.out.println("Engine version: " + anyCarModel.getEngine().getVersion());
        System.out.println("Transmission: " + anyCarModel.getTransmission().getTransmissionType());

        // ApplicationEvent sample implementation
        CarRacingEventPublisher crep = (CarRacingEventPublisher) context.getBean("carRacingEventPublisher");
        crep.publish();
        crep.publish();

        System.out.println();
        context.stop();
    }
}
