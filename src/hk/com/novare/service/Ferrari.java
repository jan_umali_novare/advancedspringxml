package hk.com.novare.service;

import hk.com.novare.model.CarModel;
import hk.com.novare.model.EngineModel;
import hk.com.novare.model.TransmissionModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("ferrari")
public class Ferrari implements carService {

    private CarModel carModel;
    private EngineModel engineModel;
    private TransmissionModel transmissionModel;

    public Ferrari(CarModel carModel, EngineModel engineModel, TransmissionModel transmissionModel) {
        this.carModel = carModel;
        this.engineModel = engineModel;
        this.transmissionModel = transmissionModel;
    }

    @Override
    public CarModel assembleCar() {
        String version = "V8";
        String engine = "8";
        String transmission = "semi-automatic";

        engineModel.setNumberOfEngines(engine);
        engineModel.setVersion(version);

        transmissionModel.setTransmissionType(transmission);

        carModel.setEngine(engineModel);
        return carModel;
    }
}
