package hk.com.novare.service;

import hk.com.novare.model.CarModel;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "hk.com.novare")
public interface carService {
    CarModel assembleCar();
}
