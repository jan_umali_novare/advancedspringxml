package hk.com.novare.service;

import hk.com.novare.model.CarModel;
import hk.com.novare.model.EngineModel;
import hk.com.novare.model.TransmissionModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("lamborghini")
public class Lamborghini implements carService {

    private CarModel carModel;
    private EngineModel engineModel;
    private TransmissionModel transmissionModel;

    public Lamborghini(CarModel carModel, EngineModel engineModel, TransmissionModel transmissionModel) {
        this.carModel = carModel;
        this.engineModel = engineModel;
        this.transmissionModel = transmissionModel;
    }

    public CarModel assembleCar() {
        String version = "V12";
        String engine = "12";
        String transmission = "dual-clutch";

        engineModel.setNumberOfEngines(engine);
        engineModel.setVersion(version);

        transmissionModel.setTransmissionType(transmission);

        carModel.setEngine(engineModel);
        return carModel;
    }
}
